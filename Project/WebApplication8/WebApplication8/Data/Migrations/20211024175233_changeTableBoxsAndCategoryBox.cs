﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication8.Data.Migrations
{
    public partial class changeTableBoxsAndCategoryBox : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "TypeBoxName",
                table: "CategoryBoxes",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CategoryBoxName",
                table: "Boxes",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TypeBoxName",
                table: "Boxes",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TypeBoxName",
                table: "CategoryBoxes");

            migrationBuilder.DropColumn(
                name: "CategoryBoxName",
                table: "Boxes");

            migrationBuilder.DropColumn(
                name: "TypeBoxName",
                table: "Boxes");
        }
    }
}
