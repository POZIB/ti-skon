﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication8.Data.Migrations
{
    public partial class changeCustomBoxItems : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IdCustomBox",
                table: "CustomBoxItems");

            migrationBuilder.DropColumn(
                name: "IdItem",
                table: "CustomBoxItems");

            migrationBuilder.AddColumn<int>(
                name: "CustomBoxId",
                table: "CustomBoxItems",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ItemId",
                table: "CustomBoxItems",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CustomBoxId",
                table: "CustomBoxItems");

            migrationBuilder.DropColumn(
                name: "ItemId",
                table: "CustomBoxItems");

            migrationBuilder.AddColumn<int>(
                name: "IdCustomBox",
                table: "CustomBoxItems",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "IdItem",
                table: "CustomBoxItems",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
