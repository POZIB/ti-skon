﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication8.Data.Migrations
{
    public partial class changeTables2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Boxes_CategoryBoxes_CategoryBoxId",
                table: "Boxes");

            migrationBuilder.DropForeignKey(
                name: "FK_BoxItems_Items_ItemId",
                table: "BoxItems");

            migrationBuilder.DropForeignKey(
                name: "FK_CategoryBoxes_TypeBoxes_TypeBoxId",
                table: "CategoryBoxes");

            migrationBuilder.DropIndex(
                name: "IX_BoxItems_ItemId",
                table: "BoxItems");

            migrationBuilder.DropIndex(
                name: "IX_Boxes_CategoryBoxId",
                table: "Boxes");

            migrationBuilder.AddColumn<int>(
                name: "CategoryId",
                table: "Items",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "TypeBoxId",
                table: "CategoryBoxes",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CategoryBoxId",
                table: "Boxes",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BoxId",
                table: "Boxes",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Boxes_BoxId",
                table: "Boxes",
                column: "BoxId");

            migrationBuilder.AddForeignKey(
                name: "FK_Boxes_CategoryBoxes_BoxId",
                table: "Boxes",
                column: "BoxId",
                principalTable: "CategoryBoxes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CategoryBoxes_TypeBoxes_TypeBoxId",
                table: "CategoryBoxes",
                column: "TypeBoxId",
                principalTable: "TypeBoxes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Boxes_CategoryBoxes_BoxId",
                table: "Boxes");

            migrationBuilder.DropForeignKey(
                name: "FK_CategoryBoxes_TypeBoxes_TypeBoxId",
                table: "CategoryBoxes");

            migrationBuilder.DropIndex(
                name: "IX_Boxes_BoxId",
                table: "Boxes");

            migrationBuilder.DropColumn(
                name: "CategoryId",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "BoxId",
                table: "Boxes");

            migrationBuilder.AlterColumn<int>(
                name: "TypeBoxId",
                table: "CategoryBoxes",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "CategoryBoxId",
                table: "Boxes",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateIndex(
                name: "IX_BoxItems_ItemId",
                table: "BoxItems",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_Boxes_CategoryBoxId",
                table: "Boxes",
                column: "CategoryBoxId");

            migrationBuilder.AddForeignKey(
                name: "FK_Boxes_CategoryBoxes_CategoryBoxId",
                table: "Boxes",
                column: "CategoryBoxId",
                principalTable: "CategoryBoxes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BoxItems_Items_ItemId",
                table: "BoxItems",
                column: "ItemId",
                principalTable: "Items",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CategoryBoxes_TypeBoxes_TypeBoxId",
                table: "CategoryBoxes",
                column: "TypeBoxId",
                principalTable: "TypeBoxes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
