﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication8.Data.Migrations
{
    public partial class changeTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Box_BoxItems_BoxItemsId",
                table: "Box");

            migrationBuilder.DropForeignKey(
                name: "FK_Box_CategoryBox_CategoryBoxId",
                table: "Box");

            migrationBuilder.DropForeignKey(
                name: "FK_BoxItems_Box_BoxId",
                table: "BoxItems");

            migrationBuilder.DropForeignKey(
                name: "FK_CategoryBox_TypeBox_TypeBoxId",
                table: "CategoryBox");

            migrationBuilder.DropForeignKey(
                name: "FK_Item_Box_BoxId",
                table: "Item");

            migrationBuilder.DropForeignKey(
                name: "FK_Item_CategoryItem_CategoryItemId",
                table: "Item");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TypeBox",
                table: "TypeBox");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Item",
                table: "Item");

            migrationBuilder.DropIndex(
                name: "IX_Item_BoxId",
                table: "Item");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CategoryItem",
                table: "CategoryItem");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CategoryBox",
                table: "CategoryBox");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Box",
                table: "Box");

            migrationBuilder.DropIndex(
                name: "IX_Box_BoxItemsId",
                table: "Box");

            migrationBuilder.DropColumn(
                name: "Titile",
                table: "TypeBox");

            migrationBuilder.DropColumn(
                name: "BoxId",
                table: "Item");

            migrationBuilder.DropColumn(
                name: "Titile",
                table: "Item");

            migrationBuilder.DropColumn(
                name: "Titile",
                table: "CategoryItem");

            migrationBuilder.DropColumn(
                name: "Titile",
                table: "CategoryBox");

            migrationBuilder.DropColumn(
                name: "BoxItemsId",
                table: "Box");

            migrationBuilder.DropColumn(
                name: "Titile",
                table: "Box");

            migrationBuilder.RenameTable(
                name: "TypeBox",
                newName: "TypeBoxes");

            migrationBuilder.RenameTable(
                name: "Item",
                newName: "Items");

            migrationBuilder.RenameTable(
                name: "CategoryItem",
                newName: "CategoryItems");

            migrationBuilder.RenameTable(
                name: "CategoryBox",
                newName: "CategoryBoxes");

            migrationBuilder.RenameTable(
                name: "Box",
                newName: "Boxes");

            migrationBuilder.RenameIndex(
                name: "IX_Item_CategoryItemId",
                table: "Items",
                newName: "IX_Items_CategoryItemId");

            migrationBuilder.RenameIndex(
                name: "IX_CategoryBox_TypeBoxId",
                table: "CategoryBoxes",
                newName: "IX_CategoryBoxes_TypeBoxId");

            migrationBuilder.RenameIndex(
                name: "IX_Box_CategoryBoxId",
                table: "Boxes",
                newName: "IX_Boxes_CategoryBoxId");

            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "TypeBoxes",
                maxLength: 200,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<int>(
                name: "CategoryItemId",
                table: "Items",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "Items",
                maxLength: 200,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "CategoryItems",
                maxLength: 200,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "CategoryBoxes",
                maxLength: 200,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<int>(
                name: "CategoryBoxId",
                table: "Boxes",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "Boxes",
                maxLength: 200,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TypeBoxes",
                table: "TypeBoxes",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Items",
                table: "Items",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CategoryItems",
                table: "CategoryItems",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CategoryBoxes",
                table: "CategoryBoxes",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Boxes",
                table: "Boxes",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_BoxItems_ItemId",
                table: "BoxItems",
                column: "ItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_Boxes_CategoryBoxes_CategoryBoxId",
                table: "Boxes",
                column: "CategoryBoxId",
                principalTable: "CategoryBoxes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BoxItems_Boxes_BoxId",
                table: "BoxItems",
                column: "BoxId",
                principalTable: "Boxes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BoxItems_Items_ItemId",
                table: "BoxItems",
                column: "ItemId",
                principalTable: "Items",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CategoryBoxes_TypeBoxes_TypeBoxId",
                table: "CategoryBoxes",
                column: "TypeBoxId",
                principalTable: "TypeBoxes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Items_CategoryItems_CategoryItemId",
                table: "Items",
                column: "CategoryItemId",
                principalTable: "CategoryItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Boxes_CategoryBoxes_CategoryBoxId",
                table: "Boxes");

            migrationBuilder.DropForeignKey(
                name: "FK_BoxItems_Boxes_BoxId",
                table: "BoxItems");

            migrationBuilder.DropForeignKey(
                name: "FK_BoxItems_Items_ItemId",
                table: "BoxItems");

            migrationBuilder.DropForeignKey(
                name: "FK_CategoryBoxes_TypeBoxes_TypeBoxId",
                table: "CategoryBoxes");

            migrationBuilder.DropForeignKey(
                name: "FK_Items_CategoryItems_CategoryItemId",
                table: "Items");

            migrationBuilder.DropIndex(
                name: "IX_BoxItems_ItemId",
                table: "BoxItems");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TypeBoxes",
                table: "TypeBoxes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Items",
                table: "Items");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CategoryItems",
                table: "CategoryItems");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CategoryBoxes",
                table: "CategoryBoxes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Boxes",
                table: "Boxes");

            migrationBuilder.DropColumn(
                name: "Title",
                table: "TypeBoxes");

            migrationBuilder.DropColumn(
                name: "Title",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "Title",
                table: "CategoryItems");

            migrationBuilder.DropColumn(
                name: "Title",
                table: "CategoryBoxes");

            migrationBuilder.DropColumn(
                name: "Title",
                table: "Boxes");

            migrationBuilder.RenameTable(
                name: "TypeBoxes",
                newName: "TypeBox");

            migrationBuilder.RenameTable(
                name: "Items",
                newName: "Item");

            migrationBuilder.RenameTable(
                name: "CategoryItems",
                newName: "CategoryItem");

            migrationBuilder.RenameTable(
                name: "CategoryBoxes",
                newName: "CategoryBox");

            migrationBuilder.RenameTable(
                name: "Boxes",
                newName: "Box");

            migrationBuilder.RenameIndex(
                name: "IX_Items_CategoryItemId",
                table: "Item",
                newName: "IX_Item_CategoryItemId");

            migrationBuilder.RenameIndex(
                name: "IX_CategoryBoxes_TypeBoxId",
                table: "CategoryBox",
                newName: "IX_CategoryBox_TypeBoxId");

            migrationBuilder.RenameIndex(
                name: "IX_Boxes_CategoryBoxId",
                table: "Box",
                newName: "IX_Box_CategoryBoxId");

            migrationBuilder.AddColumn<string>(
                name: "Titile",
                table: "TypeBox",
                type: "nvarchar(200)",
                maxLength: 200,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<int>(
                name: "CategoryItemId",
                table: "Item",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BoxId",
                table: "Item",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Titile",
                table: "Item",
                type: "nvarchar(200)",
                maxLength: 200,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Titile",
                table: "CategoryItem",
                type: "nvarchar(200)",
                maxLength: 200,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Titile",
                table: "CategoryBox",
                type: "nvarchar(200)",
                maxLength: 200,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<int>(
                name: "CategoryBoxId",
                table: "Box",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BoxItemsId",
                table: "Box",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Titile",
                table: "Box",
                type: "nvarchar(200)",
                maxLength: 200,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TypeBox",
                table: "TypeBox",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Item",
                table: "Item",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CategoryItem",
                table: "CategoryItem",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CategoryBox",
                table: "CategoryBox",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Box",
                table: "Box",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Item_BoxId",
                table: "Item",
                column: "BoxId");

            migrationBuilder.CreateIndex(
                name: "IX_Box_BoxItemsId",
                table: "Box",
                column: "BoxItemsId");

            migrationBuilder.AddForeignKey(
                name: "FK_Box_BoxItems_BoxItemsId",
                table: "Box",
                column: "BoxItemsId",
                principalTable: "BoxItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Box_CategoryBox_CategoryBoxId",
                table: "Box",
                column: "CategoryBoxId",
                principalTable: "CategoryBox",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BoxItems_Box_BoxId",
                table: "BoxItems",
                column: "BoxId",
                principalTable: "Box",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CategoryBox_TypeBox_TypeBoxId",
                table: "CategoryBox",
                column: "TypeBoxId",
                principalTable: "TypeBox",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Item_Box_BoxId",
                table: "Item",
                column: "BoxId",
                principalTable: "Box",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Item_CategoryItem_CategoryItemId",
                table: "Item",
                column: "CategoryItemId",
                principalTable: "CategoryItem",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
