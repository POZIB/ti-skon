﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication8.Data.Migrations
{
    public partial class change_table_orders : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AdditionalInformationDelivery",
                table: "Orders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AddressDeliveryCustomer",
                table: "Orders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AddressStoreFromPickUp",
                table: "Orders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FullNameCustomer",
                table: "Orders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NumberTelephoneCustomer",
                table: "Orders",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AdditionalInformationDelivery",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "AddressDeliveryCustomer",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "AddressStoreFromPickUp",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "FullNameCustomer",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "NumberTelephoneCustomer",
                table: "Orders");
        }
    }
}
