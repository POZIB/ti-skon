﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication8.Data.Migrations
{
    public partial class addUserTableAndProductTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AddressDelivery",
                table: "AspNetUsers",
                maxLength: 250,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FirstName",
                table: "AspNetUsers",
                maxLength: 250,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LastName",
                table: "AspNetUsers",
                maxLength: 250,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "CategoryItem",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Titile = table.Column<string>(maxLength: 200, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    ImageCategoryItem = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryItem", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TypeBox",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Titile = table.Column<string>(maxLength: 200, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    ImageType = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TypeBox", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CategoryBox",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Titile = table.Column<string>(maxLength: 200, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    ImageCategoryBox = table.Column<string>(nullable: true),
                    TypeBoxId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryBox", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CategoryBox_TypeBox_TypeBoxId",
                        column: x => x.TypeBoxId,
                        principalTable: "TypeBox",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BoxItems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BoxId = table.Column<int>(nullable: false),
                    ItemId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BoxItems", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Box",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Titile = table.Column<string>(maxLength: 200, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    ImageBox = table.Column<string>(nullable: true),
                    CategoryBoxId = table.Column<int>(nullable: false),
                    FullPrice = table.Column<decimal>(nullable: false),
                    BoxItemsId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Box", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Box_BoxItems_BoxItemsId",
                        column: x => x.BoxItemsId,
                        principalTable: "BoxItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Box_CategoryBox_CategoryBoxId",
                        column: x => x.CategoryBoxId,
                        principalTable: "CategoryBox",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Item",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Titile = table.Column<string>(maxLength: 200, nullable: false),
                    Country = table.Column<string>(nullable: true),
                    Brand = table.Column<string>(nullable: true),
                    Priority = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    ImageItem = table.Column<string>(nullable: true),
                    Price = table.Column<decimal>(nullable: false),
                    CountItem = table.Column<int>(nullable: false),
                    CategoryItemId = table.Column<int>(nullable: false),
                    BoxId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Item", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Item_Box_BoxId",
                        column: x => x.BoxId,
                        principalTable: "Box",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Item_CategoryItem_CategoryItemId",
                        column: x => x.CategoryItemId,
                        principalTable: "CategoryItem",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Box_BoxItemsId",
                table: "Box",
                column: "BoxItemsId");

            migrationBuilder.CreateIndex(
                name: "IX_Box_CategoryBoxId",
                table: "Box",
                column: "CategoryBoxId");

            migrationBuilder.CreateIndex(
                name: "IX_BoxItems_BoxId",
                table: "BoxItems",
                column: "BoxId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryBox_TypeBoxId",
                table: "CategoryBox",
                column: "TypeBoxId");

            migrationBuilder.CreateIndex(
                name: "IX_Item_BoxId",
                table: "Item",
                column: "BoxId");

            migrationBuilder.CreateIndex(
                name: "IX_Item_CategoryItemId",
                table: "Item",
                column: "CategoryItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_BoxItems_Box_BoxId",
                table: "BoxItems",
                column: "BoxId",
                principalTable: "Box",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Box_BoxItems_BoxItemsId",
                table: "Box");

            migrationBuilder.DropTable(
                name: "Item");

            migrationBuilder.DropTable(
                name: "CategoryItem");

            migrationBuilder.DropTable(
                name: "BoxItems");

            migrationBuilder.DropTable(
                name: "Box");

            migrationBuilder.DropTable(
                name: "CategoryBox");

            migrationBuilder.DropTable(
                name: "TypeBox");

            migrationBuilder.DropColumn(
                name: "AddressDelivery",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "FirstName",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "LastName",
                table: "AspNetUsers");
        }
    }
}
