﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using WebApplication7.Entities;
using WebApplication8.Entities;
using WebApplication8.Models;

namespace WebApplication8.Data
{
    public class ApplicationUser : IdentityUser
    {
        [Display(Name = "Имя")]
        [StringLength(250, MinimumLength = 2)]
        public string FirstName { get; set; }

        [Display(Name = "Фамилия")]
        [StringLength(250, MinimumLength = 2)]
        public string LastName { get; set; }

        [MaxLength(64)]
        [Display(Name = "Номер телефона")]
        public override string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Обязательное поле")]
        [MaxLength(64)]
        [Display(Name = "Почта")]
        [EmailAddress]
        public override string Email { get; set; }

    }


    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<TypeBox> TypeBoxes { get; set; }
        public DbSet<CategoryBox> CategoryBoxes { get; set; }
        public DbSet<Box> Boxes { get; set; }
        public DbSet<BoxItems> BoxItems { get; set; }
        public DbSet<CustomBox> CustomBoxes { get; set; }
        public DbSet<CustomBoxItems> CustomBoxItems { get; set; }
        public DbSet<CategoryItem> CategoryItems { get; set; }
        public DbSet<Item> Items{ get; set; }
        public DbSet<ShoppingCartItem> ShoppingCart { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<UserAddress> UserAddresses { get; set; }
        /*public DbSet<Coupons> Coupons { get; set; }*/

    

    }
}
