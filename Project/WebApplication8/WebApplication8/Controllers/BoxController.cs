﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplication7.Entities;
using WebApplication8.Data;

namespace WebApplication8.Controllers
{
    public class BoxController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BoxController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Boxes
        public async Task<IActionResult> Index()
        {
            return View(await _context.Boxes.ToListAsync());
        }

        // GET: Boxes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

          
            Box box = await _context.Boxes.FirstOrDefaultAsync(m => m.Id == id);

            if (box == null)
            {
                return NotFound();
            }

            List<BoxItems> boxItems = await _context.BoxItems.Where(i => i.BoxId == id).ToListAsync();
            List<Item> items = new List<Item>();

            foreach(var _item in boxItems)
            {
                var item = await _context.Items.FirstOrDefaultAsync(i => i.Id == _item.ItemId);
                items.Add(item); 
                    
            }
            box.Items = items;

            return View(box);

           
        }
      

        private bool BoxExists(int id)
        {
            return _context.Boxes.Any(e => e.Id == id);
        }
    }
}
