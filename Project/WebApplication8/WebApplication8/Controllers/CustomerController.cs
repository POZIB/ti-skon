﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication7.Entities;
using WebApplication8.Data;
using WebApplication8.Entities;
using WebApplication8.Models;

namespace WebApplication8.Controllers
{
    [Authorize]
    public class CustomerController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        public CustomerController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        
        public async Task<ActionResult> Orders(int pageLimit = 10, int page = 1)
        {
            var user = User.Identity.Name;
            var orders = await _context.Orders.Where(i => i.User == user).OrderByDescending(i => i.OrderDate).ToListAsync();
            List<Box> boxes = await _context.Boxes.ToListAsync();
            List<CustomBox> customBoxes = await _context.CustomBoxes.ToListAsync();
            List<Item> items = await _context.Items.ToListAsync();

            foreach (var order in orders)
            {
                order.OrderDetails = await _context.OrderDetails.Where(i => i.OrderId == order.Id)?.ToListAsync();
              
                foreach(var detail in order.OrderDetails)
                {
                    detail.Box = boxes.Where(i => i.Id == detail.BoxId).FirstOrDefault();
                    detail.CustomBox = customBoxes.Where(i => i.Id == detail.CustomBoxId).FirstOrDefault();
                    if(detail.CustomBoxId != 0)
                    {
                        detail.CustomBox.Items = await _context.CustomBoxItems.Where(i => i.CustomBoxId == detail.CustomBox.Id).Select(i => i.item).ToListAsync();
                    }
                    
                }
            }
         
            CustomerOrdersViewModel customerOrdersViewModel = new CustomerOrdersViewModel
            {
                User = await _context.Users.FirstOrDefaultAsync(i => i.UserName == user),
                Orders = orders             
            };
 
            return View(customerOrdersViewModel);
        }



        
        public async Task<ActionResult> CustomerData()
        {
            var _user = await _context.Users.Where(i => i.UserName == User.Identity.Name).FirstOrDefaultAsync();
            ModelState.Clear();
     
            CustomerDataViewModel customerData = new CustomerDataViewModel
            {
                FirstName = _user.FirstName,
                LastName = _user.LastName,
                Email = _user.Email,
                PhoneNumber = _user.PhoneNumber,
                Password = null,
                CurrentPassword = null,
                ConfirmPassword = null,
                ModelSuccess = new List<ModelSuccess>()
            };           
            return View(customerData);
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CustomerData(CustomerDataViewModel customerData)
        {
            customerData.ModelSuccess = new List<ModelSuccess>();

            if (ModelState.IsValid)
            {
                customerData.ModelSuccess = new List<ModelSuccess>();
                var _user = await _context.Users.Where(i => i.UserName == User.Identity.Name).FirstOrDefaultAsync();

                if (_user.FirstName != customerData.FirstName || _user.LastName != customerData.LastName || _user.Email != customerData.Email || _user.PhoneNumber != customerData.PhoneNumber)
                {
                    _user.FirstName = customerData.FirstName;
                    _user.LastName = customerData.LastName;
                    _user.Email = customerData.Email;
                    _user.NormalizedEmail = customerData.Email.ToUpper();
                    _user.PhoneNumber = customerData.PhoneNumber;
                    _context.Update(_user);
                    await _context.SaveChangesAsync();
                    ModelSuccess modelSuccess = new ModelSuccess
                    {
                        Parametr = "data",
                        Message = "Данные успешно сохранены"
                    };
                    customerData.ModelSuccess.Add(modelSuccess);
                }                           


                if (!String.IsNullOrWhiteSpace(customerData.Password) && !String.IsNullOrWhiteSpace(customerData.ConfirmPassword))
                {
                    if (_userManager.CheckPasswordAsync(_user, customerData.Password).Result)
                    {
                        ModelState.AddModelError(string.Empty, "Текущий пароль совпадает с новым!");
                        return View(customerData);                       
                    }

                    var result = await _userManager.ChangePasswordAsync(_user, customerData.CurrentPassword, customerData.Password);
                    if (result.Succeeded)
                    {
                        await _context.SaveChangesAsync();
                        ModelState.Clear();
                        customerData.Password = String.Empty;
                        customerData.CurrentPassword = String.Empty;
                        customerData.ConfirmPassword = String.Empty;
                        ModelSuccess modelSuccess = new ModelSuccess
                        {
                            Parametr = "password",
                            Message = "Пароль успешно сохранен"
                        };
                        customerData.ModelSuccess.Add(modelSuccess);
                    }
                    else
                    {                       
                        ModelState.AddModelError(string.Empty, "Неверный текущий пароль");
                    }                  
               }
            }
            return View(customerData);
        }


        
        public async Task<ActionResult> CustomerAdress()
        {
            return View( await _context.UserAddresses.Where(i => i.User == User.Identity.Name).ToListAsync());
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CustomerAdress(UserAddress userAddress)
        {
            return View(await _context.UserAddresses.Where(i => i.User == User.Identity.Name).ToListAsync());
        }
    }
}
