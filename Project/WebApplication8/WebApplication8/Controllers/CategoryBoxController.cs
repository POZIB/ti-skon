﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplication7.Entities;
using WebApplication8.Data;

namespace WebApplication8.Controllers
{
    public class CategoryBoxController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CategoryBoxController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: CategoryBox
        public async Task<IActionResult> List()
        {
            return View(await _context.CategoryBoxes.ToListAsync());
        }


        public async Task<IActionResult> Details(int id)
        {
            CategoryBox categoryBox = _context.CategoryBoxes.Where(i => i.Id == id).FirstOrDefault();
            categoryBox.Boxes = await _context.Boxes.Where(i => i.CategoryBoxId == id).ToListAsync();

            return View(categoryBox);
        }

    }
}
