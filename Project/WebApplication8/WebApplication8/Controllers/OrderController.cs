﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplication7.Entities;
using WebApplication8.Data;
using WebApplication8.Entities;
using WebApplication8.Models;

namespace WebApplication8.Controllers
{
    [Authorize]
    public class OrderController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        public OrderController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }


        public async  Task<IActionResult> Index()
        {
            List<ShoppingCartItem> shoppingCartItems = await _context.ShoppingCart.Where(i => i.User == User.Identity.Name).ToListAsync();

            if (shoppingCartItems.Count() == 0)
            {
                return RedirectToAction("Index", "Home");
            }

            List<OrderDetail> orderDetails = new List<OrderDetail>();
            List<Box> boxes = await _context.Boxes.ToListAsync();
            List<CustomBox> customBoxes = await _context.CustomBoxes.ToListAsync();
            List<Item> items = await _context.Items.ToListAsync();
            List<CustomBoxItems> customBoxItems = await _context.CustomBoxItems.ToListAsync();
            OrderDetail detail = new OrderDetail();
            decimal _totalPrice = 0;

            foreach (var element in shoppingCartItems)
            {
                if(element.BoxId != 0) { 
                    detail.Box = boxes.FirstOrDefault(i => i.Id == element.BoxId);
                    _totalPrice += element.Box.FullPrice * element.Quantity;
                }
                if (element.CustomBoxId != 0) { 
                    detail.CustomBox = customBoxes.FirstOrDefault(i => i.Id == element?.CustomBoxId);

                    detail.CustomBox.Items = customBoxItems.Where(i => i.CustomBoxId == element.CustomBoxId).Select(i => i.item).ToList();

                    _totalPrice += element.CustomBox.TotalPrice * element.Quantity;
                }
                detail.Quantity = element.Quantity;



                orderDetails.Add(detail);
                detail = new OrderDetail();
            }

            Order order = new Order
            {
                AppUser = await _userManager.GetUserAsync(User),
                OrderDetails = orderDetails,
                Total = _totalPrice,
                UserAddresses = await _context.UserAddresses.Where(i => i.User == User.Identity.Name).ToListAsync()
            };

            return View(order);
        }
        
      
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(int radio_delivery,int radio_payment,int itemUserAddress, Order order)
        {
            try
            {
          
                if (ModelState.IsValid)
                {
                    if (_context.ShoppingCart.Where(i => i.User == User.Identity.Name).Count() == 0)
                        return View(order);

                    order.DeliveryId = radio_delivery;
                    order.PaymentTransactionId = radio_payment.ToString();
                    order.UserAddressId = itemUserAddress;
                    order.OrderDate = DateTime.Now;
                    order.User = User.Identity.Name;

                    if (radio_delivery == 1)
                    {
                        var userAddresses = _context.UserAddresses.Where(i => i.Id == itemUserAddress);
                        order.FullNameCustomer = await userAddresses.Select(i => i.FirstName +" "+ i.LastName).FirstOrDefaultAsync();
                        order.AddressDeliveryCustomer = await userAddresses.Select(i => i.City +"  " + i.Street +"  дом "+ i.Home +"  кв "+ i.Apartment).FirstOrDefaultAsync();
                        order.AdditionalInformationDelivery = await userAddresses.Select(i => i.AdditionalInformation).FirstOrDefaultAsync();
                        order.NumberTelephoneCustomer = await userAddresses.Select(i => i.PhoneNumber).FirstOrDefaultAsync();
                    }
                    if (radio_delivery == 2){ 
                        order.AddressStoreFromPickUp = "---";
                    }


                    var total = await _context.ShoppingCart.Where(i => i.User == User.Identity.Name).Select(i => i.Quantity * i.Box.FullPrice).SumAsync();
                    total += await _context.ShoppingCart.Where(i => i.User == User.Identity.Name).Select(i => i.Quantity * i.CustomBox.TotalPrice).SumAsync();
                    order.Total = total;
                    order.Status = "в обработке";
                    await _context.AddAsync(order);
                    await _context.SaveChangesAsync();

                    List<ShoppingCartItem> shoppingCartItem = await _context.ShoppingCart.Where(i => i.User == User.Identity.Name).ToListAsync();
                    var boxes = await _context.Boxes.ToListAsync();
                    var customBoxes = await _context.CustomBoxes.ToListAsync();
                    foreach (var el in shoppingCartItem)
                    {
                        OrderDetail orderDetail = new OrderDetail
                        {
                            BoxId = el.BoxId,
                            CustomBoxId = el.CustomBoxId,
                            OrderId = order.Id,
                            Quantity = el.Quantity,
                            User = User.Identity.Name, 
                            UnitPrice = el.CustomBoxId != 0 ? (double)(el.Quantity * customBoxes.Where(i => i.Id == el.CustomBoxId).Select(i => i.TotalPrice).FirstOrDefault()) : (double)(el.Quantity * boxes.Where(i => i.Id == el.BoxId).Select(i => i.FullPrice).FirstOrDefault())
                        };
                        
                        _context.OrderDetails.Add(orderDetail);
                    }
                    await _context.SaveChangesAsync();

                    _context.ShoppingCart.RemoveRange(shoppingCartItem); 

                    await _context.SaveChangesAsync();

                    return RedirectToAction(nameof(OrderComplete));

                }
                return View(order);
            }
            catch (Exception e)
            {
                return View(order);
            }
           
        }

        
        public async Task<IActionResult> OrderComplete()
        {        
            return View();
        }
    }
}
