﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication7.Entities;
using WebApplication8.Data;
using WebApplication8.ViewModels;

namespace WebApplication8.Areas.Admin.Controllers
{
   
    public class ConstructorBox : Controller
    {

        private readonly ApplicationDbContext _context;

        public ConstructorBox(ApplicationDbContext context)
        {
            _context = context;
        }


        [HttpGet]
        public async Task<ActionResult> Index(int id)
        {
            var all = await _context.Items.ToListAsync();
            var cat = await _context.CategoryItems.ToListAsync();
            ConstructorBoxViewModel constructorBoxViewModel = new ConstructorBoxViewModel
            {
                ItemsToAdd = all,
                CateforyItems = cat
            };

            return View(constructorBoxViewModel);
        }


        [HttpGet]
        public async Task <ActionResult> GetItemsToAdd(int categoryItemsId = 0)
        {
            List<Item> items = categoryItemsId == 0 ? await _context.Items.ToListAsync() : await _context.Items.Where(i => i.CategoryItemId == categoryItemsId).ToListAsync();
            return View(items);
        }



    }
}
