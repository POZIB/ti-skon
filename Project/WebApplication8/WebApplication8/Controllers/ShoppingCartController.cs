﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication7.Entities;
using WebApplication8.Data;
using WebApplication8.Entities;
using WebApplication8.Models;

namespace WebApplication8.Controllers
{
    public class ShoppingCartController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ShoppingCartController(ApplicationDbContext context)
        {
            _context = context;
        }
        public const string CartSessionKey = "User";
        public string ShoppingCartId { get; set; }


        [HttpGet]
        public async Task<ActionResult> Index()
        {
            ViewBag.TotalPrice = await GetTotalPrice();
            return View(await GetCartList());
        }

       /* ShoppingCart/getcartlist */
        public async Task<IEnumerable<ShoppingCartItem>> GetCartList()
        {
            ShoppingCartId = GetCartIdByUserOrSession();
            IEnumerable<ShoppingCartItem> shoppingCarts = await _context.ShoppingCart.Where(c => c.User == ShoppingCartId).ToListAsync();
            List<Item> items = await _context.Items.ToListAsync();
            List<Box> boxes = await _context.Boxes.ToListAsync();
            List<CustomBox> customBoxes = await _context.CustomBoxes.ToListAsync();
            List<CustomBoxItems> boxItems = await _context.CustomBoxItems.ToListAsync();

            foreach (var element in shoppingCarts)
            {
                element.Box = boxes.FirstOrDefault(i => i.Id == element.BoxId);

                if(element.CustomBoxId != 0)
                {
                    element.CustomBox = customBoxes.FirstOrDefault(i => i.Id == element.CustomBoxId);
                    List<CustomBoxItems> _Boxitems = boxItems.Where(i => i.CustomBoxId == element.CustomBox?.Id).ToList();

                    List<Item> _items = new List<Item>();
                    foreach (var el in _Boxitems)
                    {
                        _items.Add(items.FirstOrDefault(i => i.Id == el.ItemId));
                    }
                    element.CustomBox.Items = _items;
                }
                
            }

            return shoppingCarts;
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async  Task<ActionResult> AddBoxToCart(int boxId)
        {
            try
            {
                ShoppingCartId = GetCartIdByUserOrSession();
                var shoppingCart = _context.ShoppingCart.SingleOrDefault(i => i.User == ShoppingCartId && i.BoxId == boxId);

                if (shoppingCart == null)
                {                
                    shoppingCart = new ShoppingCartItem
                    {                      
                        BoxId = boxId,
                        User = ShoppingCartId,
                        Box = _context.Boxes.SingleOrDefault(i => i.Id == boxId),
                        Quantity = 1,
                    };                 
                    _context.ShoppingCart.Add(shoppingCart);                   
                }
                else
                {       
                    shoppingCart.Quantity++;
                }
                await _context.SaveChangesAsync();

                return new EmptyResult();
            }
            catch
            {
                return View();
            }
        }



        [HttpPost]
        /*[ValidateAntiForgeryToken]*/
        public async Task<ActionResult> AddCustomBoxToCart(int[] arrItemsId, string srcImgBox, string titleBox = "Общий бокс")
        {
            try
            { 
                if (arrItemsId.Length < 5) return Content("массив меньше 5");

                ShoppingCartId = GetCartIdByUserOrSession();
                List<Item> items = new List<Item>();
                decimal totalPrice = 0;
                foreach (var id in arrItemsId)
                {
                    Item item = await _context.Items.FirstOrDefaultAsync(i => i.Id == id);
                    totalPrice += item.Price;
                    items.Add(item);
                }

                CustomBox customBox = new CustomBox {
                     Title = titleBox,
                     ImageBox = srcImgBox,
                     TotalPrice = totalPrice,
                     UserId = ShoppingCartId

                };

                var custBox = await _context.CustomBoxes.AddAsync(customBox);
                await _context.SaveChangesAsync();

                CustomBoxItems customBoxItems = new CustomBoxItems();
                foreach (var item in items)
                {
                    customBoxItems.CustomBoxId = customBox.Id;
                    customBoxItems.ItemId = item.Id;
                    customBoxItems.PriceItem = item.Price;
                    customBoxItems.CategoryItme = item.CategoryItemName;
                    await _context.CustomBoxItems.AddAsync(customBoxItems);
                    await _context.SaveChangesAsync();
                    customBoxItems = new CustomBoxItems();
                }

                ShoppingCartItem shoppingCart = new ShoppingCartItem
                {
                    BoxId = 0,
                    CustomBoxId = customBox.Id,
                    User = ShoppingCartId,
                    Quantity = 1,
                };
                await _context.ShoppingCart.AddAsync(shoppingCart);
                await _context.SaveChangesAsync();

                return new EmptyResult(); 
            }
            catch(Exception e)
            {
                string str = e.Message;
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangeQuantity(int itemCartId, int quantity)
        {
            /*ShoppingCart/AddToCart*/
            try
            {
                ShoppingCartId = GetCartIdByUserOrSession();
                ShoppingCartItem shoppingCart;
                if (itemCartId != 0)
                {
                    shoppingCart = await _context.ShoppingCart.SingleOrDefaultAsync(i => i.User == ShoppingCartId && i.Id == itemCartId);
                    shoppingCart.Quantity = quantity;
                }

                await _context.SaveChangesAsync();

                ViewBag.TotalPrice = await GetTotalPrice();
                return PartialView("Out", await GetCartList());
            }
            catch
            {
                return View();
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteFromCart(int itemCartId)
        {
            try
            {
                ShoppingCartId = GetCartIdByUserOrSession();

                ShoppingCartItem shoppingCart;
                if (itemCartId != 0)
                {
                    shoppingCart = await _context.ShoppingCart.SingleOrDefaultAsync(i => i.User == ShoppingCartId && i.Id == itemCartId);

                    if (shoppingCart.CustomBoxId != 0 ) {
                        CustomBox customBox = await _context.CustomBoxes.FirstOrDefaultAsync(i => i.Id == shoppingCart.CustomBoxId);
                        _context.CustomBoxes.Remove(customBox);
                        await _context.SaveChangesAsync();

                        var customBoxItems = _context.CustomBoxItems.Where(i => i.CustomBoxId == shoppingCart.CustomBoxId);
                        _context.CustomBoxItems.RemoveRange(customBoxItems);
                        await _context.SaveChangesAsync();
                    }
                    else{
                        _context.ShoppingCart.Remove(shoppingCart);
                        await _context.SaveChangesAsync();
                    }

                }

                ViewBag.TotalPrice = await GetTotalPrice();
                return PartialView("Out", await GetCartList());
            }
            catch(Exception e)
            {
                return View();
            }
        }


        [HttpGet]
        public int CountShoppingCart()
        {
            try
            {
                ShoppingCartId = GetCartIdByUserOrSession();
                int countShoppingCart = _context.ShoppingCart.Where(i => i.User == ShoppingCartId).Select(i => i.Quantity).Sum();
                return countShoppingCart;
            }
            catch
            {
                return 0;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ClearCart()
        {
            try
            {
                ShoppingCartId = GetCartIdByUserOrSession();
                List<ShoppingCartItem> shoppingCart = await _context.ShoppingCart.Where(i => i.User == ShoppingCartId).ToListAsync();
                _context.ShoppingCart.RemoveRange(shoppingCart);

                await _context.SaveChangesAsync();

                return PartialView("Out", await GetCartList());
            }
            catch
            {
                return View();
            }
        }
        /* ShoppingCart/GetCartId*/
        public string GetCartIdByUserOrSession()
        {           
            if (HttpContext.Session.GetString(CartSessionKey) == null || !string.IsNullOrWhiteSpace(HttpContext.User.Identity.Name))
            {
         
                if (!string.IsNullOrWhiteSpace(HttpContext.User.Identity.Name))
                {
                    HttpContext.Session.SetString(CartSessionKey, HttpContext.User.Identity.Name);
                }
                else
                {
                    // Generate a new random GUID using System.Guid class.     
                    Guid tempCartId = Guid.NewGuid();
                    HttpContext.Session.SetString(CartSessionKey, tempCartId.ToString());
                }
            }
            return HttpContext.Session.GetString(CartSessionKey);

        }
        public async Task<decimal> GetTotalPrice()
        {
            ShoppingCartId = GetCartIdByUserOrSession();
            decimal? total = decimal.Zero;    

            total = (decimal?) await _context.ShoppingCart.Where(i => i.User == ShoppingCartId).Select(i => i.Quantity * i.Box.FullPrice).SumAsync();
            total += (decimal?) await _context.ShoppingCart.Where(i => i.User == ShoppingCartId).Select(i => i.Quantity * i.CustomBox.TotalPrice).SumAsync();
           
            total = Math.Round(total.Value,2);
            return total ?? decimal.Zero;
        }



    }
}
