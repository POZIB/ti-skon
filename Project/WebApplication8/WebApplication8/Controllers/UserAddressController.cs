﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication8.Data;
using WebApplication8.Models;

namespace WebApplication8.Controllers
{
   
    public class UserAddressController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        public UserAddressController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _context = context;
        }


        // GET: UserAddressController
        public async Task<ActionResult> Index()
        {
            var address = await _context.UserAddresses.Where(i => i.User == User.Identity.Name).ToListAsync();
            return View(address);
        }


        public ActionResult Edit(int id)
        {
            return View(_context.UserAddresses.FirstOrDefault(i => i.Id == id && i.User == User.Identity.Name));
        }

        // POST: UserAddressController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id,[Bind("Id,City,Street,Home,Apartment,LastName,FirstName,PhoneNumber,AdditionalInformation ")] UserAddress userAddress)
        {
            try
            {
                userAddress.isValid = "true";

                if (ModelState.IsValid)
                {
                    userAddress.isValid = "";
                    userAddress.User = User.Identity.Name;
                    _context.Update(userAddress);

                    await _context.SaveChangesAsync();

                    return RedirectToAction(nameof(Index));
                }
                return View(userAddress);
            }
            catch
            {
                return View();
            }
        }


        public ActionResult Create()
        {
                return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async  Task<ActionResult> Create(int id, UserAddress userAddress)
        {
            try
            {
                userAddress.isValid = "true";

                if (ModelState.IsValid)
                {
                    userAddress.isValid = "";
                    userAddress.User = User.Identity.Name;
                    _context.UserAddresses.Add(userAddress);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    return View(userAddress);
                }

            }
            catch
            {
                return View();
            }
        }



        // POST: UserAddressController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                var address = _context.UserAddresses.FirstOrDefault(i => i.Id == id && i.User == User.Identity.Name);

                _context.UserAddresses.Remove(address);
                await _context.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
