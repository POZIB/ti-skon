﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication7.Entities;
using WebApplication8.Data;
using WebApplication8.Extensions;

namespace WebApplication8.Controllers
{ 
    public class ItemController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ItemController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Admin/Item
        public async Task<IActionResult> Details(int id)
        {                       
            return View( _context.Items.Where(i => i.Id == id).FirstOrDefault());
        }

       
    }
}
