﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication7.Entities;

namespace WebApplication8.Models
{
    public class MenuViewModel
    {
        public IEnumerable<TypeBox> typeBoxes { get; set; }
        public IEnumerable <CategoryBox> categoryBoxes { get; set; }
        public IEnumerable <Box> boxes { get; set; }
        
    }
}
