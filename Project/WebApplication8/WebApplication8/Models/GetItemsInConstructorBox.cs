﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication7.Entities;

namespace WebApplication8.Models
{
    public class GetItemsInConstructorBox
    {
        public decimal FullPrice { get; set; }

        public IEnumerable<BoxItems> BoxItems { get; set; }
    }
}
