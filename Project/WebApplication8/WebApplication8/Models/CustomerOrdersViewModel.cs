﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication7.Entities;
using WebApplication8.Data;

namespace WebApplication8.Models
{
    public class CustomerOrdersViewModel
    {
        public ApplicationUser User { get; set; }
        public IEnumerable<Order> Orders { get; set; }
        
    }
}
