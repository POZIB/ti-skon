﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using WebApplication8.Data;

namespace WebApplication8.Models
{
    public class UserAddress
    {
        public int Id { get; set; }

        public string User { get; set; }

        [Required(ErrorMessage = "Обязательное поле")]
        [StringLength(100, MinimumLength = 2)]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Обязательное поле")]
        [StringLength(100, MinimumLength = 2)]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Обязательное поле")]
        [DisplayName("Номер телефона")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Обязательное поле")]
        [DisplayName("Город")]
        [StringLength(40)]
        public string City { get; set; }

        [Required(ErrorMessage = "Обязательное поле")]
        [DisplayName("Улица")]
        [StringLength(40)]
        public string Street { get; set; }

        [Required(ErrorMessage = "Обязательное поле")]
        [DisplayName("Дом")]
        [StringLength(30)]
        public string Home { get; set; }

        [Required(ErrorMessage = "Обязательное поле")]
        [DisplayName("Квартира")]
        [StringLength(10)]
        public string Apartment { get; set; }

        [DisplayName("Дополнительная информация(необязательно)")]
        [StringLength(200)]
        public string AdditionalInformation { get; set; }

        [NotMapped]
        public ApplicationUser AppUser { get; set; }

        [NotMapped]
        public string isValid { get; set; }

    }
}
