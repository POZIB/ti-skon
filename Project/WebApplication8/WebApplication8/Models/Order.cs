﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using WebApplication7.Entities;
using WebApplication8.Data;

namespace WebApplication8.Models
{
    public class Order
    {
        public int Id { get; set; }
        public System.DateTime OrderDate { get; set; }
        public string User{ get; set; }
        public int UserAddressId { get; set; }
        public int DeliveryId { get; set; }      
      
        [ScaffoldColumn(false)]
        public decimal Total { get; set; }

        [ScaffoldColumn(false)]
        public string PaymentTransactionId { get; set; }

        [ScaffoldColumn(false)]
        public bool HasBeenShipped { get; set; }

        [ScaffoldColumn(false)]
        public string Status { get; set; }

        [ScaffoldColumn(false)]
        public string FullNameCustomer { get; set; }
       
        [ScaffoldColumn(false)]
        public string NumberTelephoneCustomer { get; set; }

        [ScaffoldColumn(false)]
        public string AddressDeliveryCustomer { get; set; }

        [ScaffoldColumn(false)]
        public string AdditionalInformationDelivery { get; set; }

        [ScaffoldColumn(false)]
        public string AddressStoreFromPickUp { get; set; }

        [ScaffoldColumn(false)]
        public List<OrderDetail> OrderDetails { get; set; }

        [NotMapped]
        public ApplicationUser AppUser { get; set; }

        [NotMapped]
        public List<UserAddress> UserAddresses { get; set; }

    }
}
