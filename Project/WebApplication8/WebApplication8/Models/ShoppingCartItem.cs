﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using WebApplication7.Entities;
using WebApplication8.Entities;

namespace WebApplication8.Models
{
    public class ShoppingCartItem
    {
        public int Id { get; set; }
        public string User { get; set; }
        public int Quantity { get; set; }
        public int BoxId { get; set; }
        public int CustomBoxId { get; set; }
        public  Box Box { get; set; }
        public CustomBox CustomBox { get; set; }

    }
}
