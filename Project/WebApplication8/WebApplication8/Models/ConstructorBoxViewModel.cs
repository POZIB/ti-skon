﻿using System;
using System.Collections.Generic;
using WebApplication7.Entities;

namespace WebApplication8.ViewModels
{
    public class ConstructorBoxViewModel
    {
        public int BoxId { get; set; }

        public string NameBox { get; set; }

        public decimal FullPrice { get; set; }

        public IEnumerable<BoxItems> BoxItems { get; set; }

        public IEnumerable<CategoryItem> CateforyItems { get; set; }
        public IEnumerable<Item> ItemsToAdd { get; set; }



    }
}
