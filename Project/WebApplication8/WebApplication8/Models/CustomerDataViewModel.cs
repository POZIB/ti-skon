﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApplication8.Data;

namespace WebApplication8.Models
{
    public class CustomerDataViewModel
    {

        [Required(ErrorMessage = "Обязательное поле")]
        [Display(Name = "Имя")]
        [StringLength(250, ErrorMessage = "{0} должно содержать от {2} до {1} символов.", MinimumLength = 2)]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Обязательное поле")]
        [Display(Name = "Фамилия")]
        [StringLength(250, ErrorMessage = "{0} должна содержать от {2} до {1} символов.", MinimumLength = 2)]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Обязательное поле")]
        [MaxLength(13, ErrorMessage = "{0} должен содержать до {1} символов.")]
        [Display(Name = "Номер телефона")]
        public string PhoneNumber { get; set; }


        [Required(ErrorMessage = "Обязательное поле")]
        [MaxLength(64, ErrorMessage = "{0} должна содержать до {1} символов.")]
        [Display(Name = "Почта")]
        [EmailAddress]
        public string Email { get; set; }


        [StringLength(100, ErrorMessage = "{0} должен содержать от {2} до {1} символов.", MinimumLength = 4)]
        [DataType(DataType.Password)]
        [Display(Name = "Текущий пароль")]
        public string CurrentPassword { get; set; }

        [StringLength(100, ErrorMessage = "{0} должен содержать от {2} до {1} символов.", MinimumLength = 4)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]

        public string Password { get; set; }


        [DataType(DataType.Password)]
        [Display(Name = "Подтвердить пароль")]
        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        public string ConfirmPassword { get; set; }

        public List<ModelSuccess> ModelSuccess { get; set; }
      
    }
}
