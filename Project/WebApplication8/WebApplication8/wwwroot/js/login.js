﻿$(function () {
    var loginButton = $("#LoginModal input[name='login']").click(onLoginClick);

    function onLoginClick(){

        var url = "/Account/Login";
        var antiForgeryToken = $("#LoginModal input[name='__RequestVerificationToken']").val();
        var email = $("#LoginModal input[name='Email']").val();
        var password = $("#LoginModal input[name='Password']").val();
        var rememberMe = $("#LoginModal input[name='RememberMe']").prop('checked');
      
        var loginModel = {
            __RequestVerificationToken: antiForgeryToken,
            Email: email,
            Password: password,
            RememberMe: rememberMe
        };

        $.ajax({
            type: "POST",
            url: url,
            data:  loginModel,           
            success: function (data) {
                var parsed = $.parseHTML(data);
                var hasError = $(parsed).find("input[name='LoginInValid']").val() == "true";
               
                if (hasError == true) {
                    $("#LoginModal").html(data);

                    

                    loginButton = $("#LoginModal input[name='login']").click(onLoginClick);
                    btnShowOrHidePassword = $('.btn-show-hide-password').click(showOrHidePassword);

                    var form = $("#LoginForm");

                    $(form).removeData("validator");
                    $(form).removeData("unobtrusiveValidation");
                    $.validator.unobtrusive.parse(form);
                }
                else
                {
                    location.href = window.location.pathname;
                }
            },
            error: function (xhr, ajaxOption, thrownError) {
                console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText)
            }

        });
    }
});