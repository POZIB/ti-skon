﻿$(function () {
    var registrationButton = $("#RegistrationModal input[name='registration']").click(onRegistrationClick);

    function onRegistrationClick() {

        var url = "/Account/Registration";

        var antiForgeryToken = $("#RegistrationModal input[name='__RequestVerificationToken']").val();

        var firstName = $("#RegistrationModal input[name='FirstName']").val();
        var email = $("#RegistrationModal input[name='Email']").val();
        var phoneNumber = $("#RegistrationModal input[name='PhoneNumber']").val();
        var password = $("#RegistrationModal input[name='Password']").val();

        var user = {
            __RequestVerificationToken: antiForgeryToken,
            FirstName: firstName,
            Email: email,
            PhoneNumber: phoneNumber,
            Password: password
        };

        $.ajax({
            type: "POST",
            url: url,
            data: user,
            success: function (data) {
                console.log(12321321321321)
                var parsed = $.parseHTML(data);
                var hasError = $(parsed).find("input[name='RegistrationInValid']").val() == "true";
                console.log(hasError)

                if (hasError == true) {
                    $("#RegistrationModal").html(data);

                    registrationButton = $("#RegistrationModal input[name='registration']").click(onRegistrationClick);
                    btnShowOrHidePassword = $('.btn-show-hide-password').click(showOrHidePassword);

                    var form = $("#RegistrationForm");

                    $(form).removeData("validator");
                    $(form).removeData("unobtrusiveValidation");
                    $.validator.unobtrusive.parse(form);
                }
                else {
                    location.href = window.location.pathname;
                }

            },
            error: function (xhr, ajaxOption, thrownError) {
                console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText)
            }

        });
    }
});