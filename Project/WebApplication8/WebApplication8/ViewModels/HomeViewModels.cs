﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication7.Entities;
using WebApplication8.Models;

namespace WebApplication8.ViewModels
{
    public class HomeViewModels
    {
        public IEnumerable<Box> Boxes { get; set; }

    }
}
