﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplication7.Entities;
using WebApplication8.Data;
using WebApplication8.Extensions;

namespace WebApplication8.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class CategoryBoxController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CategoryBoxController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Admin/CategoryBox
        public async Task<IActionResult> Index(int typeBoxId)
        {         
            if (typeBoxId == 0)
            {
                return View(await _context.CategoryBoxes.ToListAsync());
            }
            List<CategoryBox> list = await _context.CategoryBoxes.Where(i => i.TypeBoxId == typeBoxId).ToListAsync();
            ViewBag.typeBoxId = typeBoxId;
            ViewBag.SomeOrAll = typeBoxId;
            ViewBag.TitleIndex = await _context.CategoryBoxes.Where(i => i.TypeBoxId == typeBoxId).Select(i => i.TypeBoxName).FirstOrDefaultAsync();

            return View(list);
        }

    

        // GET: Admin/CategoryBox/Create
        public async Task<IActionResult> Create(int typeBoxId, int someOrAll)
        {
            List<TypeBox> typeBoxes = await _context.TypeBoxes.ToListAsync();

            CategoryBox categoryBox = new CategoryBox 
            { 
                TypeBoxId = typeBoxId,
                TypesBox = typeBoxes.ConvertToSelectList(typeBoxId)
            };
            ViewBag.typeBoxId = typeBoxId;
            ViewBag.SomeOrAll = someOrAll;

            return View(categoryBox);
        }

        // POST: Admin/CategoryBox/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(int someOrAll, [Bind("Id,Title,Description,ImageCategoryBox,TypeBoxId")] CategoryBox categoryBox)
        {
            if (ModelState.IsValid)
            {
                categoryBox.TypeBoxName = _context.TypeBoxes.Where(i => i.Id == categoryBox.TypeBoxId).Select(i => i.Title).FirstOrDefault().ToString();
                categoryBox.ImageCategoryBox = "/images/" + categoryBox.ImageCategoryBox;

                _context.Add(categoryBox);
                await _context.SaveChangesAsync();

                return RedirectToAction(nameof(Index), new { typeBoxId = someOrAll });
            }

            List<TypeBox> typeBoxes = await _context.TypeBoxes.ToListAsync();
            categoryBox.TypesBox = typeBoxes.ConvertToSelectList(categoryBox.TypeBoxId);     
            return View(categoryBox);
        }

        // GET: Admin/CategoryBox/Edit/5
        public async Task<IActionResult> Edit(int? id,int someOrAll)
        {
            if (id == null)
            {
                return NotFound();
            }

            var categoryBox = await _context.CategoryBoxes.FindAsync(id);
            if (categoryBox == null)
            {
                return NotFound();
            }

            List<TypeBox> typeBoxes = await _context.TypeBoxes.ToListAsync();
            categoryBox.TypesBox = typeBoxes.ConvertToSelectList(categoryBox.TypeBoxId);
            
            ViewBag.SomeOrAll = someOrAll;

            return View(categoryBox);
        }

        // POST: Admin/CategoryBox/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, int someOrAll, [Bind("Id,Title,Description,ImageCategoryBox,TypeBoxId")] CategoryBox categoryBox)
        {
            if (id != categoryBox.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    categoryBox.TypeBoxName = _context.TypeBoxes.Where(i => i.Id == categoryBox.TypeBoxId).Select(i => i.Title).FirstOrDefault().ToString();
                    categoryBox.ImageCategoryBox = "/images/" + categoryBox.ImageCategoryBox;
                    _context.Update(categoryBox);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CategoryBoxExists(categoryBox.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index), new { typeBoxId = someOrAll });
            }
            return View(categoryBox);
        }

        // GET: Admin/CategoryBox/Details/5
        public async Task<IActionResult> Details(int? id, int someOrAll)
        {
            if (id == null)
            {
                return NotFound();
            }

            var categoryBox = await _context.CategoryBoxes
                .FirstOrDefaultAsync(m => m.Id == id);
            if (categoryBox == null)
            {
                return NotFound();
            }
            ViewBag.SomeOrAll = someOrAll;

            return View(categoryBox);
        }

        // GET: Admin/CategoryBox/Delete/5
        public async Task<IActionResult> Delete(int? id, int someOrAll)
        {
            if (id == null)
            {
                return NotFound();
            }

            var categoryBox = await _context.CategoryBoxes
                .FirstOrDefaultAsync(m => m.Id == id);
            if (categoryBox == null)
            {
                return NotFound();
            }
            ViewBag.SomeOrAll = someOrAll;

            return View(categoryBox);
        }

        // POST: Admin/CategoryBox/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id, int someOrAll)
        {
            var categoryBox = await _context.CategoryBoxes.FindAsync(id);
            _context.CategoryBoxes.Remove(categoryBox);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index), new { typeBoxId = someOrAll });
        }

        private bool CategoryBoxExists(int id)
        {
            return _context.CategoryBoxes.Any(e => e.Id == id);
        }
    }
}
