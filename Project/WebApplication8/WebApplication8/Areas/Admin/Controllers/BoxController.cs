﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplication7.Entities;
using WebApplication8.Data;
using WebApplication8.Extensions;

namespace WebApplication8.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class BoxController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BoxController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Admin/Box
        public async Task<IActionResult> Index(int categoryBoxId)
        {
            if (categoryBoxId == 0)
            {
                return View(await _context.Boxes.ToListAsync());
            }

            List<Box> list = await _context.Boxes.Where(box => box.CategoryBoxId == categoryBoxId).ToListAsync();

            ViewBag.TitleIndexCategoryName = await _context.Boxes.Where(i => i.CategoryBoxId == categoryBoxId).Select(i => i.CategoryBoxName).FirstOrDefaultAsync();
            ViewBag.TitleIndexTypeName = await _context.Boxes.Where(i => i.CategoryBoxId == categoryBoxId).Select(i => i.TypeBoxName).FirstOrDefaultAsync();
            ViewBag.CategoryBoxId = categoryBoxId;
            ViewBag.SomeOrAll = categoryBoxId;

            return View(list);

        }


        // GET: Admin/Box/Create
        public async Task<IActionResult> Create(int categoryBoxId, int someOrAll)
        {
            List<CategoryBox> categoriesBox = await _context.CategoryBoxes.ToListAsync();

            Box box = new Box
            {
                CategoryBoxId = categoryBoxId,
                CategoriesBox = categoriesBox.ConvertToSelectList(categoryBoxId)
            };

            ViewBag.categoryBoxId = categoryBoxId;
            ViewBag.SomeOrAll = someOrAll;
            return View(box);
        }

        // POST: Admin/Box/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(int someOrAll, [Bind("Title,Description,Additionally,ImageBox,FullPrice,CategoryBoxId")] Box box)
        {
            if (ModelState.IsValid)
            {
                CategoryBox categoryBox = _context.CategoryBoxes.Where(i => i.Id == box.CategoryBoxId).FirstOrDefault();
                if (categoryBox != null)
                {
                    box.CategoryBoxName = categoryBox.Title;
                    box.TypeBoxName = categoryBox.TypeBoxName;
                    box.ImageBox = "/image/products/boxes/" + box.ImageBox;
                }

                _context.Add(box);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index), new { categoryBoxId = someOrAll });
            }
            List<CategoryBox> categoriesBox = await _context.CategoryBoxes.ToListAsync();
            box.CategoriesBox = categoriesBox.ConvertToSelectList(box.CategoryBoxId);


            return View(box);
        }

        // GET: Admin/Box/Edit/5
        public async Task<IActionResult> Edit(int? id, int someOrAll)
        {
            if (id == null)
            {
                return NotFound();
            }

            var box = await _context.Boxes.FindAsync(id);
            if (box == null)
            {
                return NotFound();
            }

            List<CategoryBox> categoriesBox = await _context.CategoryBoxes.ToListAsync();
            box.CategoriesBox = categoriesBox.ConvertToSelectList(box.CategoryBoxId);
            ViewBag.SomeOrAll = someOrAll;
            return View(box);
        }

        // POST: Admin/Box/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, int someOrAll, [Bind("Id,Title,Description,Additionally,ImageBox, FullPrice,CategoryBoxId")] Box box)
        {
            if (id != box.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    CategoryBox categoryBox = _context.CategoryBoxes.Where(i => i.Id == box.CategoryBoxId).FirstOrDefault();
                    if (categoryBox != null)
                    {
                        box.CategoryBoxName = categoryBox.Title;
                        box.TypeBoxName = categoryBox.TypeBoxName;
                        box.ImageBox = "/image/products/boxes/" + box.ImageBox;
                    }

                    _context.Update(box);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BoxExists(box.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index), new { categoryBoxId = someOrAll });
            }
            return View(box);
        }

        // GET: Admin/Box/Details/5
        public async Task<IActionResult> Details(int? id, int someOrAll)
        {
            if (id == null)
            {
                return NotFound();
            }

            var box = await _context.Boxes
                .FirstOrDefaultAsync(m => m.Id == id);
            if (box == null)
            {
                return NotFound();
            }

            ViewBag.SomeOrAll = someOrAll;
            return View(box);
        }

        // GET: Admin/Box/Delete/5
        public async Task<IActionResult> Delete(int? id, int someOrAll)
        {
            if (id == null)
            {
                return NotFound();
            }

            var box = await _context.Boxes
                .FirstOrDefaultAsync(m => m.Id == id);
            if (box == null)
            {
                return NotFound();
            }

            ViewBag.SomeOrAll = someOrAll;
            return View(box);
        }

        // POST: Admin/Box/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id, int someOrAll)
        {
            var box = await _context.Boxes.FindAsync(id);
            _context.Boxes.Remove(box);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index), new { categoryBoxId = someOrAll });
        }

        private bool BoxExists(int id)
        {
            return _context.Boxes.Any(e => e.Id == id);
        }
    }
}
