﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication8.Areas.Admin.ViewModels;
using WebApplication8.Data;

namespace WebApplication8.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class RolesController : Controller
    {
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _context;
        public object ApplicationDefaultRoles { get; private set; }

        public RolesController(RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager, ApplicationDbContext context)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _context = context;
        }
        public IActionResult Index() => View(_roleManager.Roles.ToList());


        public IActionResult Create() => View();


        public async Task<IActionResult> UserWithRole()
        {
           /* var users = _context.Users.ToList();

            var us = _context.UserRoles*/;

           /* var us = _context.UserRoles;
           
            var roles = _roleManager.Roles;


            List<UserWithRole> usersWithRole = new List<UserWithRole>();
            UserWithRole oneUserWithRoles = new UserWithRole();

            foreach (var el in us)
            {
                oneUserWithRoles.User = _userManager.Users.Where(user => user.Id == el.UserId).FirstOrDefault();
                IdentityRole r = _roleManager.Roles.Where(i => i.Id == el.RoleId).FirstOrDefault();
                oneUserWithRoles.roles.Add(r.Name.ToString()); */
                   /* var role = _roleManager.Roles.Where(i => i.Id == )*/

                /*var user = _userManager.Users.Where(i => i.Id == el. ).*/
            /*}*/
            /*var user = _userManager.Users.Where(i => i.Id == )*/


            return View();             
        }

   

       [HttpPost]
        public async Task<IActionResult> Create(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                IdentityResult result = await _roleManager.CreateAsync(new IdentityRole(name));
                if (result.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            return View(name);
        }


        [HttpPost]
        public async Task<IActionResult> Delete(string id)
        {
            IdentityRole role = await _roleManager.FindByIdAsync(id);
            if (role != null)
            {
                IdentityResult result = await _roleManager.DeleteAsync(role);
            }
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> UserListAsync() {

            List<UserWithRole> listUserWithRole = new List<UserWithRole>();
            var Users = await _userManager.Users.ToListAsync();
            var Roles = await _context.UserRoles.ToListAsync();
/*            var userRoles = await _userManager.GetRolesAsync(user);*/
            foreach (var user in Users)
            {
                UserWithRole userWithRole = new UserWithRole();
                List<string> RoleIds = Roles.Where(i => i.UserId == user.Id).Select(r => r.RoleId).ToList();

                if (RoleIds.Count() > 0)
                {
                    foreach (var id in RoleIds)
                    {
                        userWithRole.ListRoleName += await _context.Roles.Where(i => i.Id == id).Select(i => i.Name).FirstOrDefaultAsync() + " ";
                    }
                }

                userWithRole.User = user;
                listUserWithRole.Add(userWithRole);
            }

            return View(listUserWithRole);
        }


        public async Task<IActionResult> Edit(string userId)
        {
            // получаем пользователя
            ApplicationUser user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                // получем список ролей пользователя
                var userRoles = await _userManager.GetRolesAsync(user);
                var allRoles = _roleManager.Roles.ToList();
                ChangeRoleViewModel model = new ChangeRoleViewModel
                {
                    UserId = user.Id,
                    UserEmail = user.Email,
                    UserRoles = userRoles,
                    AllRoles = allRoles
                };
                return View(model);
            }

            return NotFound();
        }
        [HttpPost]
        public async Task<IActionResult> Edit(string userId, List<string> roles)
        {
            // получаем пользователя
            ApplicationUser user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                // получем список ролей пользователя
                var userRoles = await _userManager.GetRolesAsync(user);
                // получаем все роли
                var allRoles = _roleManager.Roles.ToList();
                // получаем список ролей, которые были добавлены
                var addedRoles = roles.Except(userRoles);
                // получаем роли, которые были удалены
                var removedRoles = userRoles.Except(roles);

                await _userManager.AddToRolesAsync(user, addedRoles);

                await _userManager.RemoveFromRolesAsync(user, removedRoles);

                return RedirectToAction("UserList");
            }

            return NotFound();
        }


    }
}
