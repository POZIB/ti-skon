﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplication7.Entities;
using WebApplication8.Areas.Admin.ViewModels;
using WebApplication8.Data;
using WebApplication8.Extensions;

namespace WebApplication8.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class BoxItemController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BoxItemController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Admin/BoxItem
        public async Task<IActionResult> Index(int boxId)
        {           
            if (boxId == 0) return RedirectToAction("Index", "Box");

            List<BoxItems> boxItems = await _context.BoxItems.Where(item => item.BoxId == boxId).ToListAsync();

            foreach (var el in boxItems)
            {
                el.Item = await _context.Items.FirstOrDefaultAsync(item => item.Id == el.ItemId);
            }
          
            ViewBag.TitleIndex = await _context.Boxes.Where(box => box.Id == boxId).Select(i => i.Title).FirstOrDefaultAsync();
            ViewBag.BoxId = boxId;
            return View(boxItems);
        }

        // GET: Admin/BoxItem/Create
        [HttpGet]
        public async Task<IActionResult> AddItemToBox(int boxId)
        {
            if (boxId == 0) return RedirectToAction("Index", "Box");

            int[] arrayIdItemsInBox = await _context.BoxItems.Where(i => i.BoxId == boxId).Select(i => i.ItemId).ToArrayAsync();
            
            List<Item> items = new List<Item>();           
            foreach(var id in arrayIdItemsInBox)
            {
                items.Add(_context.Items.FirstAsync(i => i.Id == id).Result);
            }

            AddingItemInBox addingItemInBox = new AddingItemInBox
            {
                BoxId = boxId,
                ItemsInBox = items,
                ItemsToAddingBox = await _context.Items.ToListAsync(),
                AllCategoriesItems = _context.CategoryItems.ToListAsync().Result.ConvertToSelectList(0)               
            };
            ViewBag.TitleIndex = await _context.Boxes.Where(box => box.Id == boxId).Select(i => i.Title).FirstOrDefaultAsync();
            return View(addingItemInBox);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddItemToBox(int[] arrItems, [Bind("Id,BoxId,ItemId")] BoxItems boxItems)
        {
            List<Item> items = new List<Item>();

            foreach(var el in arrItems)
            { 
                BoxItems l = _context.BoxItems.FirstOrDefaultAsync(i => i.BoxId == boxItems.BoxId && i.ItemId == el).Result;
                if (l == null)
                {
                    items.Add(_context.Items.Where(i => i.Id == el).FirstOrDefaultAsync().Result);
                }                                                     
            }

            if (ModelState.IsValid)
            {
                if (items.Count() == 0) return RedirectToAction(nameof(Index), new { boxId = boxItems.BoxId });

                foreach (var el in items)
                {
                    boxItems.ItemId = el.Id;
                    _context.Add(boxItems);
                    await _context.SaveChangesAsync();
                    boxItems.Id = 0;
                }         

                return RedirectToAction(nameof(Index), new { boxId = boxItems.BoxId });
            }
            return View(boxItems);
        }

        public async Task<IActionResult> GetItemsByCategoryItemId(int boxId, int categoryItemId)
        {
            int[] arrayIdItemsInBox = await _context.BoxItems.Where(i => i.BoxId == boxId).Select(i => i.ItemId).ToArrayAsync();

            List<Item> items = new List<Item>();
            foreach (var id in arrayIdItemsInBox)
            {
                items.Add(_context.Items.FirstAsync(i => i.Id == id).Result);
            }

            AddingItemInBox addingItemInBox = new AddingItemInBox
            {
                ItemsInBox = items,
                ItemsToAddingBox = (categoryItemId == 0) ? await _context.Items.ToListAsync() : await _context.Items.Where(item => item.CategoryItemId == categoryItemId).ToListAsync(),
            };

            return View(addingItemInBox);
        }

        // GET: Admin/BoxItem/Delete/5
        public async Task<IActionResult> Delete(int? boxid, int? itemId)
        {
            if (boxid == null || itemId == null)
            {
                return NotFound();
            }

            var boxItems = await _context.BoxItems
                .FirstOrDefaultAsync(m => m.ItemId == itemId && m.BoxId == boxid);
            if (boxItems == null)
            {
                return NotFound();
            }

            boxItems.Item = await _context.Items.FirstOrDefaultAsync(item => item.Id == boxItems.ItemId);

            return View(boxItems);
        }

        // POST: Admin/BoxItem/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int boxid,int itemId)
        {
            var boxItems = await _context.BoxItems.FirstOrDefaultAsync(m => m.ItemId == itemId && m.BoxId == boxid);
            if (boxItems == null)
            {
                return NotFound();
            }
            _context.BoxItems.Remove(boxItems);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index), new { boxId = boxItems.BoxId } );
        }

        private bool BoxItemsExists(int id)
        {
            return _context.BoxItems.Any(e => e.Id == id);
        }
    }
}
