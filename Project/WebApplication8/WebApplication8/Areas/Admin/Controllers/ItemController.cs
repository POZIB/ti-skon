﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication7.Entities;
using WebApplication8.Data;
using WebApplication8.Extensions;

namespace WebApplication8.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class ItemController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ItemController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Admin/Item
        public async Task<IActionResult> Index(int categoryItemId)
        {
            if (categoryItemId == 0)
            {
                return View(await _context.Items.ToListAsync());
            }

            List<Item> items = await _context.Items.Where(item => item.CategoryItemId == categoryItemId).ToListAsync();

            ViewBag.TitleIndex = await _context.Items.Where(i => i.CategoryItemId == categoryItemId).Select(i => i.CategoryItemName).FirstOrDefaultAsync();
            ViewBag.SomeOrAll = categoryItemId;
            ViewBag.CategoryItemId = categoryItemId;
            
            return View(items);

        }


        // GET: Admin/Item/Create
        public async Task<IActionResult> Create(int categoryItemId, int someOrAll)
        {
            List<CategoryItem> categoryItems = await _context.CategoryItems.ToListAsync();

            Item item = new Item
            {
                CategoryItemId = categoryItemId,               
                CategoriesItem = categoryItems.ConvertToSelectList(categoryItemId)
            };

            ViewBag.SomeOrAll = someOrAll;
            ViewBag.CategoryItemId = categoryItemId;

            return View(item);
        }

        // POST: Admin/Item/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(int someOrAll,[Bind("Title,Country,Brand,Description,ModeOfApplication,ImageItem,Price,CountItem,Priority,CategoryItemId")] Item item)
        {
            if (ModelState.IsValid)
            {
                item.CategoryItemName = _context.CategoryItems.Where(i => i.Id == item.CategoryItemId).Select(i => i.Title).FirstOrDefault().ToString();
                item.ImageItem = "/image/products/items/" + item.ImageItem;

                _context.Add(item);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index), new { categoryItemId = someOrAll });
            }
            List<CategoryItem> categoryItems = await _context.CategoryItems.ToListAsync();
            item.CategoriesItem = categoryItems.ConvertToSelectList(item.CategoryItemId);
            return View(item);
        }

        // GET: Admin/Item/Edit/5
        public async Task<IActionResult> Edit(int? id, int someOrAll)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _context.Items.FindAsync(id);
            if (item == null)
            {
                return NotFound();
            }

            List<CategoryItem> categoryItems = await _context.CategoryItems.ToListAsync();
            item.CategoriesItem = categoryItems.ConvertToSelectList(item.CategoryItemId);
            ViewBag.SomeOrAll = someOrAll;
            return View(item);
        }

        // POST: Admin/Item/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, int someOrAll, [Bind("Id,Title,Country,Brand,Description,ModeOfApplication,ImageItem,Price,CountItem,Priority,CategoryItemId")] Item item)
        {
            if (id != item.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    item.CategoryItemName = _context.CategoryItems.Where(i => i.Id == item.CategoryItemId).Select(i => i.Title).FirstOrDefault().ToString();
                    item.ImageItem = "/image/products/items/" + item.ImageItem;

                    _context.Update(item);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ItemExists(item.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index), new { categoryItemId = someOrAll });
            }

            return View(item);
        }

        // GET: Admin/Item/Details/5
        public async Task<IActionResult> Details(int? id, int someOrAll)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _context.Items
                .FirstOrDefaultAsync(m => m.Id == id);
            if (item == null)
            {
                return NotFound();
            }

            ViewBag.SomeOrAll = someOrAll;
            return View(item);
        }

        // GET: Admin/Item/Delete/5
        public async Task<IActionResult> Delete(int? id, int someOrAll)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _context.Items
                .FirstOrDefaultAsync(m => m.Id == id);
            if (item == null)
            {
                return NotFound();
            }

            ViewBag.SomeOrAll = someOrAll;
            return View(item);
        }

        // POST: Admin/Item/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id, int someOrAll)
        {
            var item = await _context.Items.FindAsync(id);
            _context.Items.Remove(item);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index), new { categoryItemId = someOrAll });
        }

        private bool ItemExists(int id)
        {
            return _context.Items.Any(e => e.Id == id);
        }
    }
}
