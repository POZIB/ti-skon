﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplication7.Entities;
using WebApplication8.Data;

namespace WebApplication8.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class TypeBoxController : Controller
    {
        private readonly ApplicationDbContext _context;
        public TypeBoxController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Admin/TypeBox
        public async Task<IActionResult> Index()
        {
            return View(await _context.TypeBoxes.ToListAsync());
        }

        // GET: Admin/TypeBox/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var typeBox = await _context.TypeBoxes
                .FirstOrDefaultAsync(m => m.Id == id);
            if (typeBox == null)
            {
                return NotFound();
            }

            return View(typeBox);
        }

        // GET: Admin/TypeBox/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Admin/TypeBox/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title,Description,ImageType")] TypeBox typeBox)
        {
            if (ModelState.IsValid)
            {
                typeBox.ImageType = "/images/" + typeBox.ImageType;

                _context.Add(typeBox);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(typeBox);
        }

        // GET: Admin/TypeBox/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var typeBox = await _context.TypeBoxes.FindAsync(id);
            if (typeBox == null)
            {
                return NotFound();
            }
            return View(typeBox);
        }

        // POST: Admin/TypeBox/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,Description,ImageType")] TypeBox typeBox)
        {
            if (id != typeBox.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    typeBox.ImageType = "/images/" + typeBox.ImageType;
                    _context.Update(typeBox);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TypeBoxExists(typeBox.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(typeBox);
        }

        // GET: Admin/TypeBox/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var typeBox = await _context.TypeBoxes
                .FirstOrDefaultAsync(m => m.Id == id);
            if (typeBox == null)
            {
                return NotFound();
            }

            return View(typeBox);
        }

        // POST: Admin/TypeBox/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var typeBox = await _context.TypeBoxes.FindAsync(id);
            _context.TypeBoxes.Remove(typeBox);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TypeBoxExists(int id)
        {
            return _context.TypeBoxes.Any(e => e.Id == id);
        }
    }
}
