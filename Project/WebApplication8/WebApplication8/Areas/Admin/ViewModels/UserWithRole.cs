﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication8.Data;

namespace WebApplication8.Areas.Admin.ViewModels
{
    public class UserWithRole
    {
        public ApplicationUser User { get; set; }
        public string RoleId { get; set; }
        public string ListRoleName { get; set; }

    }
}
