﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication7.Entities;

namespace WebApplication8.Areas.Admin.ViewModels
{
    public class AddingItemInBox
    {
        public int BoxId { get; set; }

        public List<Item> ItemsInBox { get; set; }

        public List<Item> ItemsToAddingBox { get; set; }

        public List<SelectListItem> AllCategoriesItems { get; set; }
    }
}
