﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication7.Entities;
using WebApplication8.Areas.Manager.ViewModels;
using WebApplication8.Data;
using WebApplication8.Entities;
using WebApplication8.Models;

namespace WebApplication8.Areas.Manager.Controllers
{

    [Area("Manager")]
    [Authorize(Roles = "Manager")]
    public class OrdersController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _context;

        public OrdersController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _context = context;
        }

        public async Task<ActionResult> List(int idOrder, string status, string email, string canceled, string confirmed, string processing, DateTime startDate, DateTime endDate, int page = 0, int pageSize = 10)
        {
            if (page < 0) page = 0;

            if (!String.IsNullOrEmpty(status))
            {
                await ChangeOrder(idOrder, status);
            }

            List<Order> orders = null;

            if (!String.IsNullOrEmpty(email))
            {
                email = email.Trim();
                orders = await _context.Orders.Where(i => i.User == email).ToListAsync();
            }
            if ( startDate != DateTime.MinValue || endDate != DateTime.MinValue)
            {
                if (orders == null)
                {
                    orders = await _context.Orders.ToListAsync();
                    if (startDate != DateTime.MinValue)
                        orders = orders.Where(i => i.OrderDate > startDate).ToList();
                    if (endDate != DateTime.MinValue)
                        orders = orders.Where(i => i.OrderDate <= endDate.AddDays(1)).ToList();
                }
                else
                {
                    if(startDate != DateTime.MinValue)
                        orders = orders.Where(i => i.OrderDate > startDate).ToList();
                    if (endDate != DateTime.MinValue)
                        orders = orders.Where(i => i.OrderDate <= endDate.AddDays(1)).ToList();
                }
                    
            }
            if (!String.IsNullOrEmpty(canceled) || !String.IsNullOrEmpty(confirmed) || !String.IsNullOrEmpty(processing))
            {
                if (orders == null)
                    orders = await _context.Orders.Where(i => i.Status == processing || i.Status == confirmed || i.Status == canceled).ToListAsync();
                else
                    orders = orders.Where(i => i.Status == processing || i.Status == confirmed || i.Status == canceled).ToList();
            } 
            if (orders == null)
            {
                orders = await _context.Orders.ToListAsync();
            }

            List<OrderDetail> orderDetails = await _context.OrderDetails.ToListAsync();
            List<Box> Boxes = await _context.Boxes.ToListAsync();
            List<CustomBox> CustomBoxes = await _context.CustomBoxes.ToListAsync();
            var Users = await _context.Users.ToListAsync();

            foreach (var order in orders)
            {
                order.OrderDetails = orderDetails.Where(i => i.OrderId == order.Id)?.ToList();

                foreach (var detail in order.OrderDetails)
                {
                    detail.Box = Boxes.Where(i => i.Id == detail.BoxId)?.FirstOrDefault();

                    if (detail.CustomBoxId != 0) { 
                        detail.CustomBox = CustomBoxes.Where(i => i.Id == detail.CustomBoxId).FirstOrDefault();
                        detail.CustomBox.Items = await _context.CustomBoxItems.Where(i => i.CustomBoxId == detail.CustomBoxId).Select(i => i.item).ToListAsync();
                    }

                }
                order.AppUser = Users.FirstOrDefault(i => i.Email == order.User);
            }

            double ordersCount = orders.Count();
            ManageOrdersViewModels customerOrders = new ManageOrdersViewModels()
            {
                Orders = orders.OrderByDescending(i => i.OrderDate).Skip(page * pageSize).Take(pageSize).ToList(),
                emailInput = email,
                canceledCheckBox = canceled,
                confirmedCheckBox = confirmed,
                processingCheckBox = processing,
                startDate = (startDate != DateTime.MinValue) ? startDate : DateTime.MinValue,
                endDate = (endDate != DateTime.MinValue) ? endDate : DateTime.Now,
                page = page,
                countPage = ((int)Math.Ceiling(ordersCount / pageSize))
            };
         
            return View(customerOrders);
        }

        public async Task<int> ChangeOrder(int idOrder, string status)
        {
            var order = await _context.Orders.Where(i => i.Id == idOrder).FirstOrDefaultAsync();
            order.Status = status;
            _context.Update(order);
            await _context.SaveChangesAsync();
            return 0;
        }


     

    }
}
