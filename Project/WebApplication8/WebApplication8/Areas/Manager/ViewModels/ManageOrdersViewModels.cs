﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication8.Models;

namespace WebApplication8.Areas.Manager.ViewModels
{
    public class ManageOrdersViewModels
    {
        public List<Order> Orders { get; set; }
        public  string emailInput { get; set; }
        public string canceledCheckBox { get; set; }
        public string confirmedCheckBox { get; set; }
        public string processingCheckBox { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public int pageSize { get; set; }
        public int page { get; set; }
        public int countPage { get; set; }
    }
}
