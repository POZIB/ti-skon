﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WebApplication8.Interfaces;

namespace WebApplication7.Entities
{
    public class TypeBox : IPrimaryProperties
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Обязательное поле")]
        [StringLength(200, MinimumLength = 2)]
        [Display(Name = "Заголовок")]
        public string Title { get; set; }

        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Display(Name = "Изображение")]
        public string ImageType { get; set; }

        [ForeignKey("TypeBoxId")]
        public ICollection<CategoryBox> CategoryBoxes { get; set; }
    }
}
