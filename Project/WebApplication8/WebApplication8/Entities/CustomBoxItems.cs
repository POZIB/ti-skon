﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using WebApplication7.Entities;

namespace WebApplication8.Entities
{
    public class CustomBoxItems
    {
        public int Id { get; set; }

        public int CustomBoxId { get; set; }

        public int ItemId { get; set; }

        public string CategoryItme { get; set; }

        [ScaffoldColumn(false)]
        public decimal PriceItem { get; set; }
        public Item item { get; set; }
    }
}
