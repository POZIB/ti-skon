﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using WebApplication7.Entities;

namespace WebApplication8.Entities
{
    public class CustomBox
    {
        public int Id { get; set; }

        public string UserId { get; set; }

        [Required]
        [StringLength(200, MinimumLength = 2)]
        [Display(Name = "Заголовок")]
        public string Title { get; set; }

        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Display(Name = "Изображение")]
        public string ImageBox { get; set; }

        [ScaffoldColumn(false)]
        public decimal TotalPrice { get; set; }

        [NotMapped]
        public List<Item> Items { get; set; }

    }
}
