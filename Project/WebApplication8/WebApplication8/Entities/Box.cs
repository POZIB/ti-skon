﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using WebApplication8.Interfaces;

namespace WebApplication7.Entities
{
    public class Box : IPrimaryProperties
    {
        public int Id { get; set; }

        [Required]
        [StringLength(200, MinimumLength = 2)]
        [Display(Name = "Заголовок")]
        public string Title { get; set; }

        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Display(Name = "Изображение")]
        public string ImageBox { get; set; }


        /*public string ImageBox { get; set; } признак сборки бокса*/

        [Display(Name = "Цена")]
        [Range(0, 99999999999.99)]
        public decimal FullPrice { get; set; }

        [Display(Name = "Доп. крит.")]
        public string Additionally { get; set; }

        [Display(Name = "Тип бокса")]
        public string TypeBoxName { get; set; }

        [Display(Name = "Название категории")]
        public string CategoryBoxName { get; set; }

        [ForeignKey("CategoryBox")]
        [Display(Name = "Категория бокса")]
        public int CategoryBoxId { get; set; }

        [NotMapped]
        [Display(Name = "Тип бокса")]
        public int TypeBoxId { get; set; }


        [NotMapped]
        public List<Item> Items { get; set;}

        [NotMapped]
        public virtual ICollection<SelectListItem> CategoriesBox { get; set; }
    }
}
