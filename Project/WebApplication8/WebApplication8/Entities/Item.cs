﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;



namespace WebApplication7.Entities
{
    public class Item 
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Обязательное поле")]
        [StringLength(200, MinimumLength = 2)]
        [Display(Name = "Заголовок")]
        public string Title { get; set; }

        [Display(Name = "Страна")]
        public string Country { get; set; }

        [Display(Name = "Бренд")]
        public string Brand { get; set; }

        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Display(Name = "Способ применения")]
        public string ModeOfApplication { get; set; }
        
        [Display(Name = "Изображение")]
        public string ImageItem { get; set; }

        [Required(ErrorMessage = "Обязательное поле")]
        [Display(Name = "Цена")]
        [Range(0, 99999999999.99)]
        public decimal Price { get; set; }

        [Display(Name = "Кол-во")]
        public int CountItem { get; set; }

        [Display(Name = "Приоритет")]
        public string Priority { get; set; }

        [Display(Name = "Категория(id)")]
        public int CategoryItemId { get; set; }

        [Display(Name = "Категория")]
        public string CategoryItemName { get; set; }

        [NotMapped]
        public virtual ICollection<SelectListItem> CategoriesItem { get; set; }

    }
}
