﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication8.Entities
{
    public class Coupons
    {
        public int Id { get; set; }

        public string Code { get; set; }
    
        public int Percent { get; set; }

        public decimal Amount { get; set; }

        public DateTime DataStart { get; set; }

        public DateTime DataEnd { get; set; }

    }
}
