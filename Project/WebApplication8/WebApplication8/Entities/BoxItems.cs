﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication7.Entities
{
    public class BoxItems 
    {
        public int Id { get; set; }

        [ForeignKey("BoxId")]
        [Display(Name = "Бокс")]
        public int BoxId { get; set; }

        [ForeignKey("ItemId")]
        [Display(Name = "Предмет")]
        public int ItemId { get; set; }
        
        [NotMapped]
        public virtual Item Item { get; set; }

        [NotMapped]
        public virtual List<Item> Items { get; set; }

        [NotMapped]
        public int CategoryItemId { get; set; }



    }
}
