﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using WebApplication8.Interfaces;

namespace WebApplication7.Entities
{
    public class CategoryBox : IPrimaryProperties
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Обязательное поле")]
        [StringLength(200, MinimumLength = 2)]
        [Display(Name = "Заголовок")]
        public string Title { get; set; }

        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Display(Name = "Изображение")]
        public string ImageCategoryBox { get; set; }

        [Required(ErrorMessage = "Обязательное поле")]
        [Display(Name = "Тип бокса")]
        public int TypeBoxId { get; set; }

        [Display(Name = "Тип бокса")]
        public string TypeBoxName { get; set; }


        [ForeignKey("BoxId")]
        public ICollection<Box> Boxes { get; set; }

        [NotMapped]
        public virtual ICollection<SelectListItem> TypesBox { get; set; }


    }
}
