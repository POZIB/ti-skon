﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication7.Entities;
using WebApplication8.Data;
using WebApplication8.Models;

namespace WebApplication8.Components
{
    public class ShoppingCartViewComponent : ViewComponent
    {
        private readonly ApplicationDbContext _context;
        public ShoppingCartViewComponent(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            string ShoppingCartId = GetCartIdByUserOrSession();
            ShoppingCartViewModel shoppingCartViewModel = new ShoppingCartViewModel
            {
                CountShoppingCart = _context.ShoppingCart.Where(i => i.User == ShoppingCartId).Select(i => i.Quantity).Sum()
            };

            return View(shoppingCartViewModel);
        }

        public string GetCartIdByUserOrSession()
        {
            if (HttpContext.Session.GetString("User") == null || !string.IsNullOrWhiteSpace(HttpContext.User.Identity.Name))
            {

                if (!string.IsNullOrWhiteSpace(HttpContext.User.Identity.Name))
                {
                    HttpContext.Session.SetString("User", HttpContext.User.Identity.Name);
                }
                else
                {
                    // Generate a new random GUID using System.Guid class.     
                    Guid tempCartId = Guid.NewGuid();
                    HttpContext.Session.SetString("User", tempCartId.ToString());
                }
            }
            return HttpContext.Session.GetString("User");

        }

    }
}
