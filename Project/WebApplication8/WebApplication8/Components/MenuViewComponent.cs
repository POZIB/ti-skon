﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication7.Entities;
using WebApplication8.Data;
using WebApplication8.Models;

namespace WebApplication8.Components
{
    public class MenuViewComponent : ViewComponent
    {
        private readonly ApplicationDbContext _context;
        public MenuViewComponent(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            MenuViewModel menuViewModel = new MenuViewModel
            {
                typeBoxes = await _context.TypeBoxes.ToListAsync(),
                categoryBoxes = await _context.CategoryBoxes.ToListAsync(),
                boxes = await _context.Boxes.ToListAsync()
            };

            return View(menuViewModel);
        }

    }
}
