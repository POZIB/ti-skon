$color-hover:    #ca9c12;
$font-icon:    "Material Icons";

*{
	margin: 0;
	position: 0;
	outline: none;
	user-select: none;
	color: rgb(56, 56, 56);
}

html,body{
	overflow-x: hidden;
	width: 100%;
	height: 100%;
	height: 2000px;
}

.wrapper{
	min-height: 100%;
	display: flex;
	flex-direction: column;
	background-color: rgba(55, 202, 228, 0.082);

}

main{
	flex: 1 1 auto;
}

body{background-color: #fffffff3}
a{text-decoration: none;}
li{list-style-type: none;}


header{
    width: 100%;
	height: 100px;
    position: fixed;

    .header-top{
        width: 100%;
        height: 65%;
        display: flex;
        background-color: #fff;
        justify-content: space-between;
        align-items: center;
        padding: 10px 0 5px 0px;

        .block{
            display: flex;
	        align-items: center;
            .icon{
                font-size: 18px;
                font-family: font-icon;
            }
            > a{
                display: flex;
                align-items: center;
                justify-content: center;
                margin: 0 10px 0 2px;
                font-size: 18px;
            }
            a{
                &:hover{
                    color:color-hover;
                }
            } 
        }
        &.left{
            width: 30%;
            height: 100%;
            justify-content: flex-start;
            padding-left: 20px;
            .block-left-icon-menu{
                display:none;
                &:before{
                    content: '\e5d2';
                }
            }
            .block-left-numberTel{
                &:before{
                    content: '\e0cd';
                }
            }
            .block-left-map{
                &:before{
                    content: '\e0c8';
                }

            }

        }
        &.logo{
            width: 40%;
            height: 100%;
            font-size: 350%;
            display: flex;
            font-family: 'Cinzel', serif;
            justify-content: center;
        }
        &.right{
            width: 30%;
            height: 100%;
            justify-content: flex-end;
            padding-right: 20px;
            .block-right-loginInAccount{
                &:before{
                    content: '\e853';
                }
            }
            .block-right-cart{
                &before{
                    content: '\e8cc';
                }
            }

        }

    }
    .header-bottom{
        width: 100%;
        height: 50px;
        display: flex;
        align-items: flex-start;
        background-color: #fff;
        .menu{
            width: 100%;
            display: flex;
            flex-direction: row;
            input{
                display: none;
            }
            .menu-mobile-arrow_back{
                display: none;
                &:before{
                    font-family: font-icon;
                    content: '\e5c4';
                }
            }
            .container-menu{
                width: 100%;
                height: 100%;
                margin-top: 7px;
                display: flex;
                align-items: center;
                justify-content: space-evenly;
            
                .element-container-menu{
                    input{

                    }
                    a{
                        .header-element-container{
                            font-size: 20px;
                            padding: 1px 10px 5px 10px;
                            text-transform: uppercase; 
                            font-weight: 530; 
                            cursor: pointer;
                            &:hover{
                                color:color-hover;
                            } 
                        }
                    }
                    .container-submenu{
                        position: absolute;
                        display: flex;
                        flex-direction: row;
                        justify-content: space-evenly;
                        align-items: flex-start;
                        background-color: #fff;
                        left: 0;
                        margin-top: -5px;
                        display: none; 
                        padding: 25px 25px;

                        .element-submenu{
                            height: 100%;
                            display: flex;
                            flex-direction: column;
                            justify-content: center;
                            input{

                            }
                            a{
                                .header-submenu{
                                    text-transform: uppercase; 
                                    font-weight: 550; 
                                    cursor: pointer; 

                                }
                            }
                            .submenu-content{
                                width: 100%;
	                            z-index: 0;
                                p{
                                    padding:3px;
                                    &:hover: color:color-hover;
                                }
                            }
                        }
                    }

                }
            }

        }


        &.mobile{
            display: flex;
            flex-direction: column;
            box-sizing: border-box;
            background-color: #fff;
            transform: translateX(-100%);
            position: absolute;
            overflow-y: auto;
            top: 50px;
            height: 700px;
            padding: 5px 0 20px 0;

            .menu-mobile-arrow_back{
                display: flex;
                padding: 10px;
                border-bottom: 0.2px solid gray;
                font-size: 24px;
                &:hover{
                    color: color-hover;
                }
            }

            .container-menu{
                all: initial;

                
            }

        }

    }
}